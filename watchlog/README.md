# Filtering the log file
This is the script to filter the log file and create more filters.

# Warranty & License
- This code comes with "NO WARRANTY". Use it as your own risk.
- This code is free to use in personal or commercial issues.

# Author
This code is written by
- Yan Naing Myint
- CEO, Cyber Wings Co., Ltd
